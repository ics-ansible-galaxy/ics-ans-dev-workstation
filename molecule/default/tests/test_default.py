import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('dev_workstations')


def test_installed_packages(host):
    for package in ("xfce4-session", "nomachine"):
        assert host.package(package).is_installed
